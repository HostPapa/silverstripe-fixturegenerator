<?php

class GenerateCouponsFixtureTask extends BuildTask
{
    protected $title = "Generate Coupons Fixture";

    protected $description = "Generate Fixture YML file of DataObject required to test duplicated coupon issues";

    public function run($request)
    {
        if (ENV == 'prod') {
            die("Not in prod");
        }

        ob_implicit_flush(true);

        if (!Director::is_cli()) {
            echo "<h1>This action can only be run from the Cli (Command Line Interface)</h1>";
            die();
        }

        increase_time_limit_to();
        $generateCouponsFixtureJob = new GenerateCouponsFixtureJob();

        return $generateCouponsFixtureJob->run();
    }
}
