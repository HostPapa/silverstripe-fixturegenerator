<?php

namespace HostPapa\FixtureGenerator\Dumpers;

use HostPapa\FixtureGenerator\DumperInterface;
use Symfony\Component\Yaml\Yaml as YamlDumper;

class Yaml implements DumperInterface
{
    /**
     * @var string
     */
    private $filename;
    /**
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }
    /**
     * @{@inheritdoc}
     */
    public function dump(array $data)
    {
        return file_put_contents($this->filename, YamlDumper::dump($data));
    }
}
