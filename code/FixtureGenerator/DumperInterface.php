<?php

namespace HostPapa\FixtureGenerator;

interface DumperInterface
{
    /**
     * @param array $data
     */
    public function dump(array $data);
}
