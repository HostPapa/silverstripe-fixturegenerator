<?php

class GenerateFixtureTask extends BuildTask
{
    protected $title = "Generate Fixture";

    protected $description = "Generate Fixture YML file of DataObjects for UnitTest ";

    public function run($request)
    {
        ob_implicit_flush(true);

        if (!Director::is_cli()) {
            echo "<h1>This action can only be run from the Cli (Command Line Interface)</h1>";
            die();
        }

        $brand = (!empty($request->getVar('brand'))) ? $request->getVar('brand') : null;

        $project = $request->getVar('project');
        if (empty($project)) {
            echo " 'project=hostpapa' missing.";
            die();
        }

        $fileName = $request->getVar('file');
        if (empty($fileName)) {
            echo " 'file=myfilename' missing.";
            die();
        }

        $className = $request->getVar('object');
        if (empty($className) && !$brand) {
            echo " 'object=UbersmithService' missing. or set 'brand=com' to generate an brand tree)";
            die();
        }

        $id = $request->getVar('id');
        $ids = (!empty($id)) ? array_map('intval', explode(',', $id)) : null;

        // Read relation mode from input. Options are exclude or include
        $relationMode = !empty($request->getVar('relation-mode')) ? strtolower($request->getVar('relation-mode')) : 'include';

        // Read desired relations and explode comma separate from input
        $relations = !empty($request->getVar('relations')) ? array_map('trim', explode(',', $request->getVar('relations'))) : null;

        increase_time_limit_to();

        $generateFixtureJob = new GenerateFixtureJob();

        return $generateFixtureJob->run($project, $fileName, $className, $ids, $brand, $relationMode, $relations);
    }
}
