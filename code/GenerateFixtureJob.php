<?php

use HostPapa\FixtureGenerator;

class GenerateFixtureJob extends SS_Object
{
    protected static $collectIDs = false;
    protected static $dataObjects = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function run(
        $project,
        $fileName,
        $className = null,
        $ids = null,
        $brand = null,
        string $mode = null,
        $relations = null
    ) {
        @ini_set('zlib.output_compression', 'Off');
        @ini_set('output_buffering', 'Off');
        @ini_set('output_handler', '');

        $origMode = Versioned::get_reading_mode();
        Versioned::set_reading_mode("Stage.Stage");

        if ($brand != null) {
            $this->generateFixtureForBrandTree($project, $fileName, $brand);
        } else {
            $filter = ($ids) ? ['ID' => $ids] : null;

            // Process mode string
            if ($mode == 'include') {
                $mode = FixtureGenerator\Generator::RELATION_MODE_INCLUDE;
            } else {
                // Default to exclude
                $mode = FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE;
            }

            $this->generateFixtureForDataObject($project, $fileName, $className, $filter, $mode, $relations);
        }

        Versioned::set_reading_mode($origMode);

        echo "Done!\n";
        echo "\n----------------------------------------------------------\n\n";

        die();
    }

    private function generateFixtureForBrandTree($project, $fileName, $brand_tld)
    {
        $brand = Brand::get()->filter(['BrandTLD' => $brand_tld])->limit(1)->first();
        if (!$brand) {
            die("Brand not found! '$brand'\n");
        }

        $brand_id = $brand->ID;

        $fixtures = [
            [
                'ClassName' => 'MoneyCurrency',
                'Filter' => ['Brands.ID' => $brand_id],
                'Mode' => FixtureGenerator\Generator::RELATION_MODE_INCLUDE,
                'Relations' => []
            ],
            [
                'ClassName' => 'PaymentGateway',
                'Filter' => ['Brands.ID' => $brand_id],
                'Mode' => FixtureGenerator\Generator::RELATION_MODE_INCLUDE,
                'Relations' => []
            ],
            [
                'ClassName' => 'CreditCard',
                'Filter' => ['Brands.ID' => $brand_id],
                'Mode' => FixtureGenerator\Generator::RELATION_MODE_INCLUDE,
                'Relations' => []
            ],
            [
                'ClassName' => 'Brand',
                'Filter' => ['ID' => $brand_id],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => [
                    'Brand.OpieIntranetBrands',
                    'Brand.OpieEmails',
                    'Brand.Coupons',
                    'Brand.DashboardMarketingSpots',
                    'Brand.LanderPanels',
                    'Brand.LocalizedHomePages',
                    'Brand.PiwikGoals',
                    'Brand.Translations',
                    'Brand.PricesContent',
                    'Brand.ServiceBundles',
                    'Brand.BrandFlag',
                    'Brand.Domains',
                    'Brand.UbersmithServices',
                    'Brand.Taxes',
                    'Brand.WHMConfigs'
                ]
            ],

            [
                'ClassName' => 'Tax',
                'Filter' => ['BrandID' => $brand_id, 'Active' => true],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => []
            ],
            [
                'ClassName' => 'BrandTLD',
                'Filter' => ['BrandID' => $brand_id],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => []
            ],

            [
                'ClassName' => 'DomainVendor',
                'Filter' => null,
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => [
                    'DomainVendor.Domains'
                ]
            ],
            [
                'ClassName' => 'DomainOptions',
                'Filter' => ['Domains.BrandID' => $brand_id, 'TLD' => ['ca', 'com', 'eu', 'fr', 'net']],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => [
                    'DomainOptions.Domains',
                    'DomainOptions.Categories',
                    'DomainOptions.DomainSorting'
                ]
            ],
            [
                'ClassName' => 'Domain',
                'Filter' => ['BrandID' => $brand_id, 'Active' => true, 'TLD' => ['ca', 'com', 'eu', 'fr', 'net']],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => []
            ],
            [
                'ClassName' => 'WHMConfig',
                'Filter' => ['BrandID' => $brand_id, 'Services.Code' => ['WHB-S', 'WHB-B', 'WHB-BP']],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => [
                    'WHMConfig.Services'
                ]
            ],
            [
                'ClassName' => 'UbersmithService',
                'Filter' => ['BrandID' => $brand_id, 'Active' => true, 'Code' => ['BACKUP', 'IP', 'DREG', 'DPRO', 'DPRI', 'SLB', 'SLP', 'SLE', 'WHB-S', 'WHB-B', 'WHB-BP']],
                'Mode' => FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
                'Relations' => [
                    'UbersmithService.UbersmithServiceOptions',
                    'UbersmithService.Coupons',
                    'UbersmithService.ServiceBundles',
                    'UbersmithService.ParentBundles',
                    'UbersmithService.ServiceUpgrades',
                    'UbersmithService.Bundles',
                    'UbersmithService.BundlesUpgrades',
                    'UbersmithService.Essentials',
                    'UbersmithService.EssentialsUpgrades',
                    'UbersmithService.EssentialsSelected',
                    'UbersmithService.ParentEssentials'
                ]
            ]
        ];

        $yml = fopen(SERVER_ROOT . "/{$project}/tests/_fixtures/{$fileName}-{$brand_tld}.yml", "w");

        foreach ($fixtures as $fixture) {
            $output = $this->generateFixtureForDataObject(
                $project,
                "{$fileName}-{$brand_tld}-" . $fixture['ClassName'],
                $fixture['ClassName'],
                $fixture['Filter'],
                $fixture['Mode'],
                $fixture['Relations']
            );
            if ($output) {
                $file = SERVER_ROOT . "/{$project}/tests/_fixtures/{$fileName}-{$brand_tld}-" . $fixture['ClassName'] . ".yml";
                if (file_exists($file)) {
                    fwrite($yml, file_get_contents($file));
                    unlink($file);
                }
            }
        }
    }

    private function generateFixtureForDataObject(
        $project,
        $fileName,
        $className,
        $filter = null,
        $mode = FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
        $relations = null
    ) {
        $path = SERVER_ROOT . "/{$project}/tests/_fixtures/{$fileName}.yml";
        $fixture = new FixtureGenerator\Dumpers\Yaml($path);

        try {
            $dataList = $className::get();

            if ($className::has_extension('Heyday\VersionedDataObjects\VersionedDataObject')) {
                $dataList->setDataQueryParam(['Versioned.stage' => 'Stage']);
            }

            if ($filter) {
                $dataList = $dataList->filter($filter);
            }

            if ($dataList->count() > 0) {
                echo "Found {$dataList->count()} '$className', exporting to '$path'\n";

                $fixtureGenerator = new FixtureGenerator\Generator(
                    $fixture,
                    $relations,
                    $mode
                );
                $fixtureGenerator->process($dataList);

                return true;
            } else {
                echo "Found {$dataList->count()} '$className', nothing to export...\n";
            }
        } catch (Exception $exception) {
            echo "Exception!, ClassName: '{$className}'. Nothing exported...\n";
            echo $exception->getMessage() . "\n\n";
        }

        return false;
    }
}
