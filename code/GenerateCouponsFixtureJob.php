<?php

use HostPapa\FixtureGenerator\Generator as Generator;
use HostPapa\FixtureGenerator as FixtureGenerator;

/**
 * Class GenerateCouponsFixtureJob
 *
 * @author Shannon Patterson
 * @usage php framework/cli-script.php dev/tasks/GenerateCouponsFixtureTask
 *
 * Builds a set of testing coupons and generates a fixtures file usable in unit tests
 * Used GenerateFixturesJob as a sample, pulled out a lot of the configuration options as they were not needed
 */
class GenerateCouponsFixtureJob extends SS_Object
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        @ini_set('zlib.output_compression', 'Off');
        @ini_set('output_buffering', 'Off');
        @ini_set('output_handler', '');

        if (ENV == 'prod') {
            die("Not in prod");
        }

        $this->setupValidCoupons();

        $this->setupVPSCoupons();
        $this->setupResellerCoupons();

        $this->setupOverlappingCoupon();

        $this->setupExpiredCoupon();
        $this->setupUnstartedCoupon();

        $this->setupTLDCoupon();

        $this->generateUniqueCoupons();

        $this->generatePromoEventTroubleCoupons();

        $this->generateCouponFixture(
            "hostpapa-coupons",
            Generator::RELATION_MODE_INCLUDE,
            [
                'Coupon.Brands',
                'Coupon.Services',
                'Coupon.UniqueCoupons',
                'UbersmithService.Brand'
            ]
        );
    }

    /**
     * @param string $fileName
     * @param string $mode
     * @param array|null $relations
     * @return bool
     */
    private function generateCouponFixture(
        string $fileName,
        string $mode = FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
        array $relations = null
    ) {
        $path = SERVER_ROOT . "/hostpapa/tests/_fixtures/{$fileName}.yml";
        $fixture = new FixtureGenerator\Dumpers\Yaml($path);

        try {
            /**
             * @var DataList $coupons
             */
            $coupons = Coupon::get();

            if ($coupons->count() > 0) {
                echo "Found {$coupons->count()} 'Coupon', exporting to '{$path}'\n";

                $fixtureGenerator = new FixtureGenerator\Generator(
                    $fixture,
                    $relations,
                    $mode
                );
                $fixtureGenerator->process($coupons);

                return true;
            } else {
                echo "Found {$coupons->count()} 'Coupon', nothing to export...\n";
            }
        } catch (Exception $exception) {
            echo "Exception!, ClassName: 'Coupon'. Nothing exported...\n";
            echo $exception->getMessage() . "\n\n";
        }

        return false;
    }

    /**
     * Creates a number of test coupons.  See internal comments for a description of each coupon
     *
     * @throws Exception on DataObject save and add errors
     */
    private function setupValidCoupons()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa'
            ]);

        $sharedHostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'WHB-B', 'WHB-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $wordPressHostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['OWP-S', 'OWP-B', 'OWP-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $essentialsPlans = UbersmithService::get()
            ->filter([
                'Code' => ['SLB', 'BACKUP', 'ASSL2', 'JET', 'DPRI'],
                'BrandID' => $brands->column('ID')
            ]);

        // clear all coupons
        $coupons = Coupon::get();
        foreach ($coupons as $coupon) {
            $coupon->delete();
        }
        echo "Deleted all existing coupons \n";

        // clear all unique coupons
        $uniqueCoupons = UniqueCoupon::get();
        foreach ($uniqueCoupons as $uniqueCoupon) {
            $uniqueCoupon->delete();
        }
        echo "Deleted all unique coupons \n";

        /**
         * Create a coupon that only works for Shared Hosting
         */
        $sharedCoupon = new Coupon();
        $sharedCoupon->Active = 1;
        $sharedCoupon->PartnerCoupon = 'SharedOnly';
        $sharedCoupon->Type = 'DiscountCash';
        $sharedCoupon->CouponCode12 = 10;
        $sharedCoupon->CouponCode24 = 10;
        $sharedCoupon->CouponCode36 = 10;
        $sharedCoupon->Description = "$10 off shared hosting";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $sharedCoupon->Brands()->add($brand);
            }
        }

        foreach ($sharedHostingPlans as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca') {
                $sharedCoupon->Services()->add($sharedHostingPlan);
            }
        }
        $sharedCoupon->write();

        echo "Created Shared Hosting coupon 'SharedOnly'. \n";

        /**
         * Create a coupon that only works for WordPress Hosting
         */
        $wordPressCoupon = new Coupon();
        $wordPressCoupon->Active = 1;
        $wordPressCoupon->PartnerCoupon = 'WordPressOnly';
        $wordPressCoupon->Type = 'DiscountCash';
        $wordPressCoupon->CouponCode12 = 15;
        $wordPressCoupon->CouponCode24 = 15;
        $wordPressCoupon->CouponCode36 = 15;
        $wordPressCoupon->Description = "$15 off wordpress hosting";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $wordPressCoupon->Brands()->add($brand);
            }
        }

        foreach ($wordPressHostingPlans as $wordPressHostingPlan) {
            if ($wordPressHostingPlan->Brand()->BrandTLD == 'ca') {
                $wordPressCoupon->Services()->add($wordPressHostingPlan);
            }
        }
        $wordPressCoupon->write();

        echo "Created WordPress Hosting coupon 'WordPressOnly'. \n";

        /**
         * Create a Duplicate Coupon
         *
         * Two coupon records with the same name
         * but different discounts per coupon
         *
         * Apply one coupon to Shared Hosting
         * Apply one coupon to WordPress Hosting
         */

        $dupCouponShared = new Coupon();
        $dupCouponShared->Active = 1;
        $dupCouponShared->PartnerCoupon = 'HostingDiscount';
        $dupCouponShared->Type = 'DiscountCash';
        $dupCouponShared->CouponCode12 = 20;
        $dupCouponShared->CouponCode24 = 20;
        $dupCouponShared->CouponCode36 = 20;
        $dupCouponShared->Description = "$20 off Shared hosting";


        $dupCouponWordPress = new Coupon();
        $dupCouponWordPress->Active = 1;
        $dupCouponWordPress->PartnerCoupon = 'HostingDiscount';
        $dupCouponWordPress->Type = 'DiscountCash';
        $dupCouponWordPress->CouponCode12 = 25;
        $dupCouponWordPress->CouponCode24 = 25;
        $dupCouponWordPress->CouponCode36 = 25;
        $dupCouponWordPress->Description = "$25 off WordPress hosting";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $dupCouponShared->Brands()->add($brand);
                $dupCouponWordPress->Brands()->add($brand);
            }
        }

        foreach ($sharedHostingPlans as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca') {
                $dupCouponShared->Services()->add($sharedHostingPlan);
            }
        }

        foreach ($wordPressHostingPlans as $wordPressHostingPlan) {
            if ($wordPressHostingPlan->Brand()->BrandTLD == 'ca') {
                $dupCouponWordPress->Services()->add($wordPressHostingPlan);
            }
        }

        $dupCouponShared->write();
        $dupCouponWordPress->write();

        echo "Created duplicate 'HostingDiscount' coupon for WP and Shared hosting.\n";

        /**
         * Create a coupon for Website Essentials
         * These are:
         * * SLB
         * * BACKUP
         * * ASSL2
         * * JET
         * * DPRI
         */

        $essentialsCoupon = new Coupon();
        $essentialsCoupon->Active = 1;
        $essentialsCoupon->PartnerCoupon = 'EssentialsDiscount';
        $essentialsCoupon->Type = 'DiscountCash';
        $essentialsCoupon->CouponCode12 = 5;
        $essentialsCoupon->CouponCode24 = 5;
        $essentialsCoupon->CouponCode36 = 5;
        $essentialsCoupon->Description = "$5 off any add-on service code";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $essentialsCoupon->Brands()->add($brand);
            }
        }

        foreach ($essentialsPlans as $essentialsPlan) {
            if ($essentialsPlan->Brand()->BrandTLD == 'ca') {
                $essentialsCoupon->Services()->add($essentialsPlan);
            }
        }

        $essentialsCoupon->write();
        echo "Created essentials coupon 'EssentialsDiscount'. \n";

        /**
         * Create a coupon which applies to all services and all brands
         * This will be done by filling Brands() and Services() with all active
         * associations instead of by leaving them blank.  Both types need to be confirmed
         */

        $allAssociatedCoupon = new Coupon();
        $allAssociatedCoupon->Active = 1;
        $allAssociatedCoupon->PartnerCoupon = 'EverythingAssociated';
        $allAssociatedCoupon->Type = 'DiscountCash';
        $allAssociatedCoupon->CouponCode12 = 7;
        $allAssociatedCoupon->CouponCode24 = 7;
        $allAssociatedCoupon->CouponCode36 = 7;
        $allAssociatedCoupon->Description = "$7 off anything (done by clicking All Brands, All Services)";

        foreach ($brands as $brand) {
            if (in_array($brand->BrandTLD, ['ca', 'com'])) {
                $allAssociatedCoupon->Brands()->add($brand);
            }
        }

        foreach (UbersmithService::get() as $ubersmithServicePlan) {
            if (in_array($ubersmithServicePlan->Brand()->BrandTLD, ['ca', 'com'])) {
                $allAssociatedCoupon->Services()->add($ubersmithServicePlan);
            }
        }

        $allAssociatedCoupon->write();
        echo "Created everything coupon 'EverythingAssociated'. \n";

        /**
         * Create a coupon which applies to all services and brands
         * This one will be done through leaving Brands() and Services() blank.
         */

        $allUnassociatedCoupon = new Coupon();
        $allUnassociatedCoupon->Active = 1;
        $allUnassociatedCoupon->PartnerCoupon = 'EverythingUnassociated';
        $allUnassociatedCoupon->Type = 'DiscountCash';
        $allUnassociatedCoupon->CouponCode12 = 8;
        $allUnassociatedCoupon->CouponCode24 = 8;
        $allUnassociatedCoupon->CouponCode36 = 8;
        $allUnassociatedCoupon->Description = "$8 off anything (done by not attaching services and brands)";

        $allUnassociatedCoupon->write();
        echo "Created everything coupon 'EverythingUnassociated'. \n";

        /**
         * Create a duplicated coupon which is tied to one of Starter, Business or Business Pro
         * for both WordPress and Shared hosting
         */

        $starterCoupon = new Coupon();
        $starterCoupon->Active = 1;
        $starterCoupon->PartnerCoupon = 'PerPlanDiscount';
        $starterCoupon->Type = 'DiscountCash';
        $starterCoupon->CouponCode12 = 44;
        $starterCoupon->CouponCode24 = 44;
        $starterCoupon->CouponCode36 = 44;
        $starterCoupon->Description = "$44 off Starter WP or Shared";

        $businessCoupon = new Coupon();
        $businessCoupon->Active = 1;
        $businessCoupon->PartnerCoupon = 'PerPlanDiscount';
        $businessCoupon->Type = 'DiscountCash';
        $businessCoupon->CouponCode12 = 55;
        $businessCoupon->CouponCode24 = 55;
        $businessCoupon->CouponCode36 = 55;
        $businessCoupon->Description = "$55 off Business WP or Shared";

        $businessProCoupon = new Coupon();
        $businessProCoupon->Active = 1;
        $businessProCoupon->PartnerCoupon = 'PerPlanDiscount';
        $businessProCoupon->Type = 'DiscountCash';
        $businessProCoupon->CouponCode12 = 66;
        $businessProCoupon->CouponCode24 = 66;
        $businessProCoupon->CouponCode36 = 66;
        $businessProCoupon->Description = "$66 off Business WP or Shared";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $starterCoupon->Brands()->add($brand);
                $businessCoupon->Brands()->add($brand);
                $businessProCoupon->Brands()->add($brand);
            }
        }

        foreach ($sharedHostingPlans->filter(['Code' => 'WHB-S']) as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca') {
                $starterCoupon->Services()->add($sharedHostingPlan);
            }
        }
        foreach ($sharedHostingPlans->filter(['Code' => 'WHB-B']) as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca') {
                $businessCoupon->Services()->add($sharedHostingPlan);
            }
        }
        foreach ($sharedHostingPlans->filter(['Code' => 'WHB-BP']) as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca') {
                $businessProCoupon->Services()->add($sharedHostingPlan);
            }
        }

        foreach ($wordPressHostingPlans->filter(['Code' => 'OWP-S']) as $wordPressHostingPlan) {
            if ($wordPressHostingPlan->Brand()->BrandTLD == 'ca') {
                $starterCoupon->Services()->add($wordPressHostingPlan);
            }
        }
        foreach ($wordPressHostingPlans->filter(['Code' => 'OWP-B']) as $wordPressHostingPlan) {
            if ($wordPressHostingPlan->Brand()->BrandTLD == 'ca') {
                $businessCoupon->Services()->add($wordPressHostingPlan);
            }
        }
        foreach ($wordPressHostingPlans->filter(['Code' => 'OWP-BP']) as $wordPressHostingPlan) {
            if ($wordPressHostingPlan->Brand()->BrandTLD == 'ca') {
                $businessProCoupon->Services()->add($wordPressHostingPlan);
            }
        }

        $starterCoupon->write();
        $businessCoupon->write();
        $businessProCoupon->write();

        echo "Created per-plan coupon 'PerPlanDiscount' which applies to Shared and WP hosting plans.\n";
    }

    /**
     * Sets up a coupon which only has a Promo Event Service code in it
     */
    private function generatePromoEventTroubleCoupons()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa'
            ]);

        $starterHostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'OWP-S'],
                'BrandID' => $brands->column('ID')
            ]);

        /**
         * Create a coupon that only works for Starter Hosting
         */
        $starterHosting = new Coupon();
        $starterHosting->Active = 1;
        $starterHosting->PartnerCoupon = 'PromoStarter';
        $starterHosting->Type = 'DiscountCash';
        $starterHosting->CouponCode12 = 10;
        $starterHosting->CouponCode24 = 10;
        $starterHosting->CouponCode36 = 10;
        $starterHosting->Description = "$10 off starter hosting -- WP and Shared";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca' || $brand->BrandTLD == 'com') {
                $starterHosting->Brands()->add($brand);
            }
        }

        foreach ($starterHostingPlans as $sharedHostingPlan) {
            if ($sharedHostingPlan->Brand()->BrandTLD == 'ca' || $sharedHostingPlan->Brand()->BrandTLD == 'com') {
                $starterHosting->Services()->add($sharedHostingPlan);
            }
        }

        $starterHosting->write();
    }

    /**
     * Sets up two coupon code with a duplicate name.
     *
     * One coupon code will be attached to Shared and Wordpress,
     * The other coupon code will be attached to Business Pro only (Shared and WP).
     *
     * This should result in the duplicate coupon warnings in the Price Handler
     */
    private function setupOverlappingCoupon()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa',
                'BrandTLD' => 'ca'
            ]);

        $sharedHostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'WHB-B', 'WHB-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $wordPressHostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['OWP-S', 'OWP-B', 'OWP-BP'],
                'BrandID' => $brands->column('ID')
            ]);


        $invalidCoupon = new Coupon();
        $invalidCoupon->Active = 1;
        $invalidCoupon->PartnerCoupon = 'InvalidCoupon';
        $invalidCoupon->Type = 'DiscountCash';
        $invalidCoupon->CouponCode12 = 77;
        $invalidCoupon->CouponCode24 = 77;
        $invalidCoupon->CouponCode36 = 77;
        $invalidCoupon->Description = "$77 off all shared and wordpress hosting";

        $businessProCoupon = new Coupon();
        $businessProCoupon->Active = 1;
        $businessProCoupon->PartnerCoupon = 'InvalidCoupon';
        $businessProCoupon->Type = 'DiscountCash';
        $businessProCoupon->CouponCode12 = 74;
        $businessProCoupon->CouponCode24 = 74;
        $businessProCoupon->CouponCode36 = 74;
        $businessProCoupon->Description = "$74 off Business for WP or Shared hosting";

        foreach ($brands as $brand) {
            $invalidCoupon->Brands()->add($brand);
            $businessProCoupon->Brands()->add($brand);
        }

        foreach ($sharedHostingPlans as $sharedHostingPlan) {
            $invalidCoupon->Services()->add($sharedHostingPlan);

            if ($sharedHostingPlan->Code == 'WHB-BP') {
                $businessProCoupon->Services()->add($sharedHostingPlan);
            }
        }

        foreach ($wordPressHostingPlans as $wordPressHostingPlan) {
            $invalidCoupon->Services()->add($wordPressHostingPlan);

            if ($wordPressHostingPlan->Code == 'OWP-BP') {
                $businessProCoupon->Services()->add($wordPressHostingPlan);
            }
        }

        $businessProCoupon->write();
        $invalidCoupon->write();
    }

    /**
     * Sets up a coupon with a start and end date that are in 2018.
     * For unit testing, this coupon will always be expired
     */
    private function setupExpiredCoupon()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa',
                'BrandTLD' => 'ca'
            ]);

        $hostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'WHB-B', 'WHB-BP', 'OWP-S', 'OWP-B', 'OWP-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $expiredCoupon = new Coupon();
        $expiredCoupon->Active = 1;
        $expiredCoupon->PartnerCoupon = 'ExpiredCoupon';
        $expiredCoupon->Type = 'DiscountCash';
        $expiredCoupon->CouponCode12 = 14;
        $expiredCoupon->CouponCode24 = 14;
        $expiredCoupon->CouponCode36 = 14;
        $expiredCoupon->Description = "$14 off all shared and wordpress hosting";

        $expiredCoupon->CouponStartDate = '2018-01-01';
        $expiredCoupon->CouponExpirationDate = '2018-12-31';

        foreach ($brands as $brand) {
            $expiredCoupon->Brands()->add($brand);
        }

        foreach ($hostingPlans as $hostingPlan) {
            $expiredCoupon->Services()->add($hostingPlan);
        }

        $expiredCoupon->write();
    }

    /**
     * Sets up a coupon with a start and end date that are in 2020.
     * For unit testing, this coupon will always be not started yet
     */
    private function setupUnstartedCoupon()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa',
                'BrandTLD' => 'ca'
            ]);

        $hostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'WHB-B', 'WHB-BP', 'OWP-S', 'OWP-B', 'OWP-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $unstartedCoupon = new Coupon();
        $unstartedCoupon->Active = 1;
        $unstartedCoupon->PartnerCoupon = 'UnstartedCoupon';
        $unstartedCoupon->Type = 'DiscountCash';
        $unstartedCoupon->CouponCode12 = 16;
        $unstartedCoupon->CouponCode24 = 16;
        $unstartedCoupon->CouponCode36 = 16;
        $unstartedCoupon->Description = "$16 off all shared and wordpress hosting";

        $unstartedCoupon->CouponStartDate = '2020-01-01';
        $unstartedCoupon->CouponExpirationDate = '2020-12-31';

        foreach ($brands as $brand) {
            $unstartedCoupon->Brands()->add($brand);
        }

        foreach ($hostingPlans as $hostingPlan) {
            $unstartedCoupon->Services()->add($hostingPlan);
        }

        $unstartedCoupon->write();
    }

    private function setupVPSCoupons()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa'
            ]);

        $vpsPlans = UbersmithService::get()
            ->filter([
                'Code' => [
                    'VPSC-LINUX-1',
                    'VPSC-LINUX-2',
                    'VPSC-LINUX-3',
                    'VPSC-LINUX-4',
                    'VPSC-LINUX-5'
                ],
                'BrandID' => $brands->column('ID')
            ]);

        $vpsCoupon = new Coupon();
        $vpsCoupon->Active = 1;
        $vpsCoupon->PartnerCoupon = 'VPSCoupon';
        $vpsCoupon->Type = 'DiscountCash';
        $vpsCoupon->CouponCode12 = 12;
        $vpsCoupon->CouponCode24 = 12;
        $vpsCoupon->CouponCode36 = 12;
        $vpsCoupon->Description = "$12 off VPS hosting";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $vpsCoupon->Brands()->add($brand);
            }
        }

        foreach ($vpsPlans as $vpsPlan) {
            if ($vpsPlan->Brand()->BrandTLD == 'ca') {
                $vpsCoupon->Services()->add($vpsPlan);
            }
        }
        $vpsCoupon->write();
    }

    private function setupResellerCoupons()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa'
            ]);

        $resellerPlans = UbersmithService::get()
            ->filter([
                'Code' => ['RBR', 'RGO', 'RPL', 'RSI', 'RTI'],
                'BrandID' => $brands->column('ID')
            ]);

        $resellerCoupon = new Coupon();
        $resellerCoupon->Active = 1;
        $resellerCoupon->PartnerCoupon = 'ResellerCoupon';
        $resellerCoupon->Type = 'DiscountCash';
        $resellerCoupon->CouponCode12 = 17;
        $resellerCoupon->CouponCode24 = 17;
        $resellerCoupon->CouponCode36 = 17;
        $resellerCoupon->Description = "$17 off Reseller hosting";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $resellerCoupon->Brands()->add($brand);
            }
        }

        foreach ($resellerPlans as $resellerPlan) {
            if ($resellerPlan->Brand()->BrandTLD == 'ca') {
                $resellerCoupon->Services()->add($resellerPlan);
            }
        }
        $resellerCoupon->write();
    }

    private function setupTLDCoupon()
    {
        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa'
            ]);

        $tldCoupon = new Coupon();
        $tldCoupon->Active = 1;
        $tldCoupon->PartnerCoupon = 'TLDCoupon';
        $tldCoupon->Type = 'DiscountCash';
        $tldCoupon->CouponCode12 = 9;
        $tldCoupon->CouponCode24 = 9;
        $tldCoupon->CouponCode36 = 9;
        $tldCoupon->Description = "$9 off TLDs .blog, .business, and .academy";

        foreach ($brands as $brand) {
            if ($brand->BrandTLD == 'ca') {
                $tldCoupon->Brands()->add($brand);
            }
        }

        $tldCoupon->DomainTLDs = 'blog|business|academy';

        $tldCoupon->write();
    }

    private function generateUniqueCoupons()
    {
        $coupon = Coupon::get()
            ->filter([
                'PartnerCoupon' => 'EverythingAssociated'
            ])
            ->first();

        $controller = new ToolsCouponsManager_Controller();

        $data = [
            'partnerCoupon' => $coupon->ID,
            'couponStartDate' => '2019-02-01',
            'couponExpirationDate' => '2020-01-31',
            'numberUniqueCoupon' => 20
        ];

        $controller->doUniqueCouponForm($data, '');
    }
}
