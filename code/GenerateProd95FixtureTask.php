<?php

class GenerateProd95FixtureTask extends BuildTask
{
    protected $title = "Generate Fixtures file for PROD-95";

    protected $description = "Generate Fixture YML file of DataObject required to test PROD-95";

    public function run($request)
    {
        if (ENV == 'prod') {
            die("Not in prod");
        }

        ob_implicit_flush(true);

        if (!Director::is_cli()) {
            echo "<h1>This action can only be run from the Cli (Command Line Interface)</h1>";
            die();
        }

        increase_time_limit_to();
        $generateProd95FixtureJob = new GenerateProd95FixtureJob();

        return $generateProd95FixtureJob->run();
    }
}
