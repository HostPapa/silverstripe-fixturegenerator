<?php

use HostPapa\FixtureGenerator\Generator as Generator;
use HostPapa\FixtureGenerator as FixtureGenerator;

/**
 * Class GenerateProd95FixtureJob
 *
 * @author Shannon Patterson
 * @usage php framework/cli-script.php dev/tasks/GenerateProd95FixtureTask
 *
 * Builds a set of testing data and generates a fixtures file usable in unit tests
 */
class GenerateProd95FixtureJob extends SS_Object
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        @ini_set('zlib.output_compression', 'Off');
        @ini_set('output_buffering', 'Off');
        @ini_set('output_handler', '');

        if (ENV == 'prod') {
            die("Not in prod");
        }

        $couponIDs = [];

        $brands = Brand::get()
            ->filter([
                'Title:StartsWith' => 'HostPapa',
                'BrandTLD' => ['ca', 'com']
            ]);

        $hostingPlans = UbersmithService::get()
            ->filter([
                'Code' => ['WHB-S', 'WHB-B', 'WHB-BP', 'OWP-S', 'OWP-B', 'OWP-BP'],
                'BrandID' => $brands->column('ID')
            ]);

        $coupon = new Coupon();

        $coupon->Active = 1;
        $coupon->PartnerCoupon = 'COUPON-WHB';
        $coupon->Type = 'DiscountCash';
        $coupon->CouponCode12 = 10;
        $coupon->CouponCode24 = 10;
        $coupon->CouponCode36 = 10;
        $coupon->Description = "$10 off Shared Hosting";

        // attach coupon to CA and COM
        foreach ($brands as $brand) {
            $coupon->Brands()->add($brand);
        }

        // attach coupon to CA and COM Shared Hosting plans
        foreach ($hostingPlans as $sharedHostingPlan) {
            if ($sharedHostingPlan->UberCategory == 'Web Hosting') {
                $coupon->Services()->add($sharedHostingPlan);
            }
        }

        $coupon->write();

        echo "Created Shared Hosting coupon 'COUPON-WHB'. \n";

        $couponIDs[] = $coupon->ID;

        /**
         * Create a coupon for WordPress
         */
        $coupon = new Coupon();

        $coupon->Active = 1;
        $coupon->PartnerCoupon = 'COUPON-OWP';
        $coupon->Type = 'DiscountCash';
        $coupon->CouponCode12 = 10;
        $coupon->CouponCode24 = 10;
        $coupon->CouponCode36 = 10;
        $coupon->Description = "$10 off WordPress Hosting";

        // attach coupon to CA and COM
        foreach ($brands as $brand) {
            $coupon->Brands()->add($brand);
        }

        // attach coupon to CA and COM Shared Hosting plans
        foreach ($hostingPlans as $sharedHostingPlan) {
            if ($sharedHostingPlan->UberCategory == 'Optimized WordPress Hosting') {
                $coupon->Services()->add($sharedHostingPlan);
            }
        }

        $coupon->write();

        echo "Created WordPress Hosting coupon 'COUPON-OWP'. \n";

        $couponIDs[] = $coupon->ID;

        /**
         * Create a coupon that is on associated on Brand
         */
        $coupon = new Coupon();

        $coupon->Active = 1;
        $coupon->PartnerCoupon = 'COUPON-ALL';
        $coupon->Type = 'DiscountCash';
        $coupon->CouponCode12 = 10;
        $coupon->CouponCode24 = 10;
        $coupon->CouponCode36 = 10;
        $coupon->Description = "$10 off all services";

        // attach coupon to CA and COM
        foreach ($brands as $brand) {
            $coupon->Brands()->add($brand);
        }

        $coupon->write();

        echo "Created Shared Hosting coupon 'COUPON-ALL'. \n";

        $couponIDs[] = $coupon->ID;

        $this->generateFixtureFile(
            $couponIDs,
            Generator::RELATION_MODE_INCLUDE,
            [
                'Coupon.Brands',
                'Coupon.Services',
                'UbersmithService.Brand'
            ]
        );
    }

    /**
     * @param array $couponIDs
     * @param string $mode
     * @param array|null $relations
     * @return bool
     */
    private function generateFixtureFile(
        array $couponIDs,
        string $mode = FixtureGenerator\Generator::RELATED_OBJECT_EXCLUDE,
        array $relations = null
    ) {
        $path = SERVER_ROOT . "/Order_Forms/tests/_fixtures/upsell-service-fixtures.yml";
        $fixture = new FixtureGenerator\Dumpers\Yaml($path);

        try {
            /**
             * @var DataList $coupons
             */
            $coupons = Coupon::get()
                ->filter([
                    'ID' => $couponIDs
                ]);

            if ($coupons->count() > 0) {
                echo "Found {$coupons->count()} 'Coupon', exporting to '{$path}'\n";

                $fixtureGenerator = new FixtureGenerator\Generator(
                    $fixture,
                    $relations,
                    $mode
                );
                $fixtureGenerator->process($coupons);

                return true;
            } else {
                echo "Found {$coupons->count()} 'Coupon', nothing to export...\n";
            }
        } catch (Exception $exception) {
            echo "Exception!, ClassName: 'Coupon'. Nothing exported...\n";
            echo $exception->getMessage() . "\n\n";
        }

        return false;
    }
}
