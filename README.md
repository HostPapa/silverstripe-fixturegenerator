# SilverStripe Fixture Generator

Allows the generation of SilverStripe unit test fixtures from existing DataObjects either programatically created or from the database.

Creating fixtures files for unit tests is tedious at best, and this library's goal is to alleviate some of the pain.

# Usage

Using the cli we can generate yml fixture for any DataObject or All DataObject related to an specific Hostpapa Brand.

This will generate an YML file for the EU brand of hostpapa, it will contain some Domain and UbersmithService for the unittest.

    php framework/cli-script.php dev/tasks/GenerateFixtureTask project=hostpapa file=hostpapa brand=eu

To generate an specific dataobject with matching ID. If no ID is set, then all records will be saved in the yml.

    php framework/cli-script.php dev/tasks/GenerateFixtureTask project=hostpapa file=hostpapa object=UbersmithService id=2,5,6

